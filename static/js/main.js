"use strict";

(function() {
        function navigationClick(event) {
            document.getElementById("nav-links")
                .classList.toggle("visible");
            event.preventDefault();
        }

        function initMenu() {
            document.getElementById("navbar-menu-toggle")
                .addEventListener("click", navigationClick, false);
        }

        // smooth scrolling navbar links
        function initSmoothScroll() {
            function scrollTo(element, to, duration) {
                if (duration <= 0) return;
                var difference = to - element.scrollTop;
                var perTick = difference / duration * 10;

                setTimeout(function () {
                    element.scrollTop = element.scrollTop + perTick;
                    if (element.scrollTop === to) return;
                    scrollTo(element, to, duration - 10);
                }, 10);
            }
            document.querySelectorAll("#nav-menu a").forEach(function (anchorLink) {
                anchorLink.addEventListener("click", function (e) {
                    //for non JS - you're welcome, Julian
                    e.preventDefault();
                    var anchorTarget = document.getElementById(anchorLink.dataset.target);
                    scrollTo(document.scrollingElement, anchorTarget.offsetTop, 500);
                    document.getElementById("nav-links").classList.remove("visible");
                });
            });
        }

        // make each menu link active when target section is in viewport
        function initActiveNavLink() {
            window.addEventListener("scroll", function () {
                var contactLink = document.querySelector('a[data-target="contact"]');
                var whyJoinLink = document.querySelector('a[data-target="why-join"]');
                var scheduleLink = document.querySelector('a[data-target="schedule"]');
                var speakersLink = document.querySelector('a[data-target="speakers"]');

                contactLink.classList.remove("active");
                whyJoinLink.classList.remove("active");
                scheduleLink.classList.remove("active");
                speakersLink.classList.remove("active");

                var scrollTop = document.scrollingElement.scrollTop;
                var scrollBottom = scrollTop + window.innerHeight;
                var contact = document.getElementById("contact");
                var whyJoin = document.getElementById("why-join");
                var schedule = document.getElementById("schedule");
                var speakers = document.getElementById("speakers");

                if (scrollTop > contact.offsetTop - 100 || scrollBottom >= contact.offsetTop + contact.offsetHeight) {
                    contactLink.classList.add("active");
                } else if (scrollTop > schedule.offsetTop - 100) {
                  scheduleLink.classList.add("active");
                } else if (scrollTop > speakers.offsetTop - 100) {
                    speakersLink.classList.add("active");
                } else if (scrollTop > whyJoin.offsetTop - 100) {
                    whyJoinLink.classList.add("active");
                }
            });
        }
        // smooth scrolling for anchor links in other pages
        function initScrollToAnchor() {
            function scrollToAnchor(element, to, duration) {
                if (duration <= 0) return;
                var difference = to - element.scrollTop - 80; // added "- 80" so target is not hidden under navbar
                var perTick = difference / duration * 10;

                setTimeout(function () {
                    element.scrollTop = element.scrollTop + perTick;
                    if (element.scrollTop === to) return;
                    scrollToAnchor(element, to, duration - 10);
                }, 10);
            }
            document.querySelectorAll(".anchor").forEach(function (articleAnchorLink) {
                articleAnchorLink.addEventListener("click", function (e) {
                    //for non JS - you're welcome, Julian
                    e.preventDefault();
                    var articleAnchorTarget = document.getElementById(articleAnchorLink.dataset.target);
                    scrollToAnchor(document.scrollingElement, articleAnchorTarget.offsetTop, 500);
                });
            });
        }
    if (document.getElementById("nav-links")) {
        initMenu();
        initSmoothScroll();
        initActiveNavLink();
    }
    initScrollToAnchor();
})();
